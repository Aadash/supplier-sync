<?php

namespace App\Supplier;

use App\Exception\InvalidParserException;

/**
 * Class Supplier3
 * @author Adam Slipiko <adam.slipiko@icloud.com>
 * @package App\Supplier
 */
class Supplier3 extends SupplierAbstract
{
    /**
     * @return string
     */
    public static function getName(): string
    {
        return 'supplier3';
    }

    /**
     * @return string
     */
    public static function getResponseType(): string
    {
        return 'json';
    }

    /**
     * @return array
     * @throws InvalidParserException
     */
    protected function parseResponse(): array
    {
        return $this->parser->parse($this->getResponse() ?: '');
    }

    /**
     * @return string|bool
     */
    protected function getResponse(): string|bool
    {
        return file_get_contents('http://localhost/suppliers/supplier3.json');
    }
}
