<?php

namespace App\Supplier;

use App\Event\GetProductsEvent;
use App\Event\IntegrationEvents;
use App\Exception\InvalidParserException;
use App\Parser\ParserInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class SupplierAbstract implements SupplierInterface
{
    protected ParserInterface $parser;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(ParserInterface $parser, EventDispatcherInterface $eventDispatcher)
    {
        $this->parser = $parser;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return array
     * @throws InvalidParserException
     * @throws Exception
     */
    abstract protected function parseResponse(): array;

    /**
     * @return string
     */
    abstract public static function getName(): string;

    public function getProducts(): array
    {
        $products = $this->parseResponse();
        $this->eventDispatcher->dispatch(new GetProductsEvent($products, $this->getName()), IntegrationEvents::SUPPLIER_GET_PRODUCTS);
        return $products;
    }
}
