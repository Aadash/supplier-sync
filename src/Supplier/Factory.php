<?php

namespace App\Supplier;

use App\Event\IntegrationEvents;
use App\Exception\SupplierNotFoundException;
use App\Listener\ProductsListener;
use App\Parser\FactoryInterface as ParserFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Factory
 * @package App\Supplier
 */
class Factory implements FactoryInterface
{
    const SUPPLIER_1 = 'supplier1';
    const SUPPLIER_2 = 'supplier2';
    const SUPPLIER_3 = 'supplier3';

    /**
     * @var ParserFactoryInterface
     */
    protected ParserFactoryInterface $parserFactory;

    /**
     * @var EventDispatcherInterface
     */
    protected EventDispatcherInterface $eventDispatcher;

    /**
     * Factory constructor.
     * @param ParserFactoryInterface $parserFactory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ParserFactoryInterface $parserFactory, EventDispatcherInterface $eventDispatcher)
    {
        $this->parserFactory = $parserFactory;
        $this->eventDispatcher = $eventDispatcher;

        $this->eventDispatcher->addListener(
            IntegrationEvents::SUPPLIER_GET_PRODUCTS,
            [new ProductsListener(), 'logProducts']
        );
    }

    /**
     * @param string $supplierName
     * @return SupplierInterface
     * @throws SupplierNotFoundException
     */
    public function getSupplier(string $supplierName): SupplierInterface
    {
        return match ($supplierName) {
            self::SUPPLIER_1 => new Supplier1($this->parserFactory->getParser(self::SUPPLIER_1), $this->eventDispatcher),
            self::SUPPLIER_2 => new Supplier2($this->parserFactory->getParser(self::SUPPLIER_2), $this->eventDispatcher),
            self::SUPPLIER_3 => new Supplier3($this->parserFactory->getParser(self::SUPPLIER_3), $this->eventDispatcher),
            default => throw new SupplierNotFoundException('Supplier not found'),
        };
    }
}
