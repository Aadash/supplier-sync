<?php

namespace App\Parser;

use App\Exception\InvalidParserException;
use Exception;

/**
 * Class JsonParser
 * @author Adam Slipiko <adam.slipiko@icloud.com>
 * @package App\Parser
 */
class JsonParser implements ParserInterface
{
    /**
     * @var string
     */
    protected string $productNodeName;
    /**
     * @var array
     */
    protected array $fieldNames;

    /**
     * @throws Exception
     */
    public function __construct(string $productNodeName, array $fieldNames)
    {
        $this->productNodeName = $productNodeName;
        $this->fieldNames = $fieldNames;
    }

    /**
     * @return string
     */
    public static function getType(): string
    {
        return 'json';
    }

    /**
     * @param string $content
     * @return array
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        $data = json_decode($content, true);
        if(!$data) {
            throw new InvalidParserException('Invalid JSON content');
        }

        $data = !empty($data[$this->productNodeName]) ? $data[$this->productNodeName] : reset($data);

        $result = [];
        $iterator = 0;
        foreach($data as $item) {
            foreach($this->fieldNames as $fieldName) {
                $result[$iterator][] = $item[$fieldName] ?? '';
            }
            $iterator++;
        }

        return $result;
    }
}
