<?php

namespace App\Parser;

use App\Exception\InvalidParserException;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class XmlParser
 * @author Adam Slipiko <adam.slipiko@icloud.com>
 * @package App\Parser
 */
class XmlParser implements ParserInterface
{

    /**
     * @var string
     */
    protected string $productNodeName;
    /**
     * @var array
     */
    protected array $fieldNames;

    /**
     * @throws Exception
     */
    public function __construct(string $productNodeName, array $fieldNames)
    {
        $this->productNodeName = $productNodeName;
        $this->fieldNames = $fieldNames;
    }

    /**
     * Returns parser type
     * @return string
     */
    public static function getType(): string
    {
        return 'xml';
    }

    /**
     * @param string $content
     * @return array
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        $this->validateContent($content);
        $crawler = new Crawler($content);

        return $crawler->filter($this->productNodeName)->each(function (Crawler $node, $i) {
            $result = [];

            foreach($this->fieldNames as $field) {
                $result[] = $node->filter($field)->text();
            }

            return $result;
        });
    }

    /**
     * XML content validation
     * @param string $content
     * @throws InvalidParserException
     */
    protected function validateContent(string $content): void
    {
        libxml_use_internal_errors(true);
        $doc = simplexml_load_string($content);

        if(!empty(libxml_get_errors())) {
            throw new InvalidParserException('Invalid XML content');
        }
    }
}
