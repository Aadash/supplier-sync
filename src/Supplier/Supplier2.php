<?php

namespace App\Supplier;

use App\Exception\InvalidParserException;

/**
 * Class Supplier2
 * @author Adam Slipiko <adam.slipiko@icloud.com>
 * @package App\Supplier
 */
class Supplier2 extends SupplierAbstract
{
    /**
     * @return string
     */
    public static function getName(): string
    {
        return 'supplier2';
    }

    /**
     * @return string
     */
    public static function getResponseType(): string
    {
        return 'xml';
    }

    /**
     * @return array
     * @throws InvalidParserException
     */
    protected function parseResponse(): array
    {
        return $this->parser->parse($this->getResponse() ?: '');
    }

    /**
     * @return string|bool
     */
    protected function getResponse(): string|bool
    {
        return file_get_contents('http://localhost/suppliers/supplier2.xml');
    }
}
