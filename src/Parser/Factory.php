<?php

namespace App\Parser;

use App\Exception\ParserNotFoundException;
use Exception;

/**
 * Class Factory
 * @author Adam Slipiko <adam.slipiko@icloud.com>
 * @package App\Parser
 */
class Factory implements FactoryInterface
{
    const SUPPLIER_1 = 'supplier1';
    const SUPPLIER_2 = 'supplier2';
    const SUPPLIER_3 = 'supplier3';

    /**
     * @param string $type
     * @return ParserInterface
     * @throws ParserNotFoundException
     */
    public function getParser(string $type): ParserInterface
    {
        return match ($type) {
            self::SUPPLIER_1 => new XmlParser('product', ['id', 'name', 'desc']),
            self::SUPPLIER_2 => new XmlParser('item', ['key', 'title', 'description']),
            self::SUPPLIER_3 => new JsonParser('', ['id', 'name']),
            default => throw new ParserNotFoundException('Parser not found'),
        };
    }
}
